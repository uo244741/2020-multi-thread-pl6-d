/*
 * Main.cpp
 *  Contraste ampliado. Contrast stretch
 * PL6-D
 * Laura Marcela Sánchez Peláez 
 * Pedro Zahonero Mangas
 *
 *  Created on: Fall 2020
 */
 
#include <CImg.h>
#include <math.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>
using namespace cimg_library;

const int NTIMES =  6;


// Data type for image components <float>
typedef float data_t;

const char* SOURCE_IMG      = "bailarina.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";

const int NUM_HILOS = 4; // Constante que almacena el número de hilos del sistema.


// Definición de la estructura
struct _IndexPixels {
	int pixelInicial; // Indice del primer pixel a procesar
	int pixelFinal; // Indice del último pixel a procesar.
};
typedef struct _IndexPixels Indices;

// Definición de la estructura que se pasará como parámetro al método que ejecutará cada hilo
struct _Parametros {
	Indices indices;

	float * pRsrc;
	float * pGsrc;
	float * pBsrc;

	float * pRnewC;
	float * pGnewC;
	float * pBnewC;

	float *rmin; 
	float *gmin; 
	float *bmin; 
	float *rmax; 
	float *gmax; 
	float *bmax;

};

typedef struct _Parametros Parametros;

// Cabecera del algoritmo de procesamiento de la imagen, que usarán cada
// uno de los hilos.
void* algoritmo_Procesamiento(void *datos);

int main() {


	/***************************************************
			Open file and object initialization
	 ************************************************/

	CImg<float> A(SOURCE_IMG); // Image A

	//Image A
	float *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
	int width, height; // Width and height of the image
	int nComp; // Number of image components

	//Image C (Salida)
	float *pRnewC, *pGnewC, *pBnewC;
	float *pdstImageC; // Pointer to the new image pixels


	struct timespec tStart, tEnd;
	double tiempo;
	// *********************************************************
	// 				INICIALIZACIÓN DE LAS VARIABLES
	// *********************************************************

	time_t now = time(0); // Hora actual para indicar si le versión ejecutada es la última.
	char *dt = ctime(&now);
	printf("VERSIÓN MULTIHILO\n Hora: %s", dt);


	/***************************************************
			Variables initialization.
	 ************************************************/

	A.display(); // Displays the source image
	width  = A.width(); // Getting information from the source image
	height = A.height();
	nComp  = A.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)

	int npixels = height * width;// Cálculo del número de pixeles.

	// Allocate memory space for destination image components
	pdstImageC = (float *) malloc (width * height * nComp * sizeof(float));
	if (pdstImageC == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	// Pointers to the RGB arrays of the Image A
	pRsrc = A.data(); // pRcomp points to the R component
	pGsrc = pRsrc + npixels; // pGcomp points to the G component
	pBsrc = pGsrc + npixels; // pBcomp points to B component

	// Pointers to the RGB arrays of the destination image
	pRnewC = pdstImageC;// pRcomp points to the R component
	pGnewC= pRnewC + npixels;// pGcomp points to the G component
	pBnewC= pGnewC + npixels;// pBcomp points to B component


	printf("Multithread\n--------------------\n");

	/***********************************************
	 * Algorithm start.
	 * Measure initial time
	 ************************************************/

	
	printf("Comienzo de la medición:  \n");
	// clock_gettime(CLOCK_REALTIME,&tStart);
	if(clock_gettime(CLOCK_REALTIME, &tStart)==-1)
	{
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	/************************************************
	 * We calculate the maximum and minimum of each RGB
	 *
	*********************************************** */
	
		// variables funcion 
	float rmin = 255; 
	float gmin = 255; 
	float bmin = 255; 
	float rmax = 0; 
	float gmax = 0; 
	float bmax = 0; 

	float r;
	float g;
	float b; 

	Parametros p; // Creamos la estructura.

	for (int n = 0; n < NTIMES; n++) { // Bucle para repetir el algoritmo TIMES veces, y obtener resultados significativos.

		// Definición del vector de hilos y del vector de estructuras.
		pthread_t  thread[NUM_HILOS];
		Indices indexArray[NUM_HILOS];
		// Vector de estructuras para almacenar la estructuras con los parámetros que se pasarán a cada hilo.
		Parametros params[NUM_HILOS];

		int reparto = width / NUM_HILOS; // Nº de píxeles asociados a cada hilo.
		int cont = 0; // Contador para ir almacenando el reparto.


		for(int i=0; i<npixels; i++){
				r = pRsrc[i];
				g = pGsrc[i];
				b = pBsrc[i];

				if (rmin > r) rmin = r;
				if (gmin > g) gmin = g;
				if (bmin > b) bmin = b;

				if (rmax < r) rmax = r;
				if (gmax < g) gmax = g;
				if (bmax < b) bmax = b;

		}

		p.rmin = &rmin; 
		p.gmin = &gmin;
		p.bmin = &bmin;

		p.rmax = &rmax;
		p.gmax = &gmax;
		p.bmax = &bmax; 

		for (int j = 0; j < height; j++){ // BUCLE PARA RECORRER CADA LÍNEA.

		// 	1.) Inicializar el vector de estructuras, rellenando cada estructura con su valor correspondiente.
			for (int i = 0; i < NUM_HILOS; i++){
				Indices index; // Creamos la estructura
				indexArray[i] = index; // Almacenamos la estructura en el array

				indexArray[i].pixelInicial = cont; // Valor del índice inicial

				if (i == NUM_HILOS - 1 ){ // Si estamos en el último hilo...
					indexArray[i].pixelFinal = (width*(j+1)) - 1 ; // Le asignamos como último índice, el ancho de la imagen.
					cont = indexArray[i].pixelFinal + 1; // Si estamos en el último hilo, asignamos al contador el valor del píxel final
														// de la línea + 1.
				} else {
					indexArray[i].pixelFinal = (cont + reparto) - 1; // Valor del índice final
					cont += reparto; // Incrementamos el contador.
				}


			}
				
			// Aquí se inicializa la estructura que contendrá los parámetros a pasarle a la función de procesamiento.
			for (int i = 0; i < NUM_HILOS; i++) {
				
				p.pRsrc = pRsrc; // Almacenamos las componentes de la imagen ORIGEN.
				p.pGsrc = pGsrc;
				p.pBsrc = pBsrc;

				p.pRnewC = pRnewC; // Almacenamos las componentes de la imagen DESTINO.
				p.pGnewC = pGnewC;
				p.pBnewC = pBnewC;

				p.indices = indexArray[i]; // Almacenamos la estructura de índices correspondiente.


				params[i] = p; // Almacenamos la estructura de los parámetros en el vector de estructuras.
			}
		// 2.) Crear cada uno de los hilos a partir del vector de hilos, pasarle un puntero a la función de procesado y
		// y un puntero a la estructura de datos correspondiente.
			for (int k = 0; k < NUM_HILOS; k++){
				if (pthread_create(&thread[k], NULL, algoritmo_Procesamiento, &params[k]) != 0){ // Se crea el hilo.
					fprintf(stderr, "ERROR durante la creación del hilo %d\n", k);
					return EXIT_FAILURE;
				}
			}

			for (int k = 0; k < NUM_HILOS; k++){
				pthread_join(thread[k], NULL); // Esperamos a que todos los hilos terminen de procesar la imagen.
			}
		}

	}

	/***********************************************
	 * 		End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 ************************************************/
	 
	//clock_gettime(CLOCK_REALTIME,&tEnd);
	if(clock_gettime(CLOCK_REALTIME, &tEnd)==-1)
	{
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}

	printf("Fin de la medición.\n");
	printf("Número de iteraciones realizadas: %d\n", NTIMES);

	tiempo = (tEnd.tv_sec - tStart.tv_sec );
	tiempo += (tEnd.tv_nsec - tStart.tv_nsec )/1e+9;
	printf("Elapsed time    :%f \n",tiempo);

	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<float> dstImageC(pdstImageC, width, height, 1, nComp);

	// Store destination image in disk
	dstImageC.save(DESTINATION_IMG); 

	// Display destination image
	dstImageC.display();

	return 0;
}

// ALgoritmo de procesamiento que engloba la parte de procesado de la imagen.
//  Recibe un puntero a una estructura que contiene todos los parámetros necesarios para llevar a cabo
// las operaciones con los píxeles.

void* algoritmo_Procesamiento(void *datos) {

	Parametros p;
	p = *(Parametros *)datos; // Hacemos un cast para obtener la estructura con los parámetros.
	int pixelInicial = p.indices.pixelInicial;
	int pixelFinal = p.indices.pixelFinal;

	


// Obtención de los parámetros


		for(int i = pixelInicial; i<= pixelFinal; i++){  


			p.pRnewC[i]  = (((p.pRsrc[i]) - *p.rmin)/(*p.rmax-*p.rmin))*255;
			p.pGnewC[i]  =(((p.pGsrc[i]) - *p.gmin)/(*p.gmax-*p.gmin))*255;
			p.pBnewC[i]  =(((p.pBsrc[i] )- *p.bmin)/(*p.bmax-*p.bmin))*255;

			// rojo f255.0 (cast float)
			if(p.pRnewC[i]  > 255){
				p.pRnewC[i]  =255;
			}
			if(p.pRnewC[i] <0){
				p.pRnewC[i]  =0;
			}
			

			// verde
			if(p.pGnewC[i] >255){
				p.pGnewC[i]=255;
			}
			if(p.pGnewC[i] <0){
				p.pGnewC[i]=0;
			} 
		
			//azul
			if(p.pBnewC[i]  >255){
				p.pBnewC[i] =255;
			}
			if(p.pBnewC[i]  <0){
				p.pBnewC[i] =0;
			}

			

	
}
return NULL;
}